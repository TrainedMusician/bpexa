import os
import plotly
import plotly.graph_objs as go
from GenBankParser import GeneBankParser


def fileReader(fileName):
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return sorted(content)


def chartCreator(data):
    hypotheticalList, annotatedList, labels = [[], [], []]
    for organism in data:
        hypotheticalList.append(organism.strip("\n").split()[1])
        annotatedList.append(organism.strip("\n").split()[2])
        labels.append(organism.strip("\n").split()[0])

    hypothetical = go.Bar(
        x=labels,
        y=hypotheticalList,
        name='Annotated',
        marker=dict(color='rgb(0,255,0)')
    )
    annotated = go.Bar(
        x=labels,
        y=annotatedList,
        name='Hypothetical',
        marker = dict(color='rgb(255,0,0)')
    )
    return [hypothetical, annotated]


if __name__ == "__main__":
    patricParser = GeneBankParser()
    for organism in ["GS5", "NCTC", "UA159", "LAR01", "NG8",
                     "s.mutans_pacbio_annotation", "Lj23", "NN2025"]:
        patricParser.getStats("../annotations/Streptococcus mutans %s.gb" %
                              organism)
    os.system("./patricParser.sh")
    fileOut = open("patricStats.txt")
    content = fileOut.readlines()
    fileOut.close()
    hypothetical = chartCreator(content)

    data = chartCreator(content)
    layout = go.Layout(
        barmode='stack',
                   xaxis=dict(type='category', title="Organism code"),
        yaxis=dict(title="Protein count")

    )
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(
        fig, filename='Barchart_annotated_vs_hypothetical_Patric.html')
