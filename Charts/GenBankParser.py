from Bio import SeqIO


class GeneBankParser:
    def __init__(self):
        self.ECNumbers = [[], [], [], [], [], []]

    def appendInfo(self, filename):
        # get all sequence records for the specified genbank file
        recs = [rec for rec in SeqIO.parse(filename, "genbank")]
        for rec in recs:
            feats = [feat for feat in rec.features if feat.type == "CDS"]
            for feat in feats:
                if feat.qualifiers.get("EC_number", [None])[0] != None:
                    self.ECNumbers[int(feat.qualifiers.get("EC_number", [None])[0][0])-1].append(feat.qualifiers.get("EC_number", [None])[0])

    def getECFrom(self, filename):
        """
        This function cleans the ECNumbers and fetches all the EC's of the
        specific file given.
        :param filename: file to parse for EC numbers, String.
        :return: list of EC's, a nested list for each category.
        """
        self.ECNumbers = [[], [], [], [], [], []]
        self.appendInfo(filename)
        return self.ECNumbers
