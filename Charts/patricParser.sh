#!/usr/bin/env bash

file="patricStats.txt"

[ -e ${file} ] && rm ${file}

FILES=../annotations/*
for f in ${FILES}
do
  echo "Processing ${f} file..."
  # take action on each file. $f store current file name
  total=$(cat "${f}" | egrep "/product=" | wc -l)
  hypo=$(cat "${f}" | egrep "/product=.*hypothetical.*" | wc -l)
  name=$(echo "${f}" | awk '{print($NF)}')
  printf "${name}\t${total}\t${hypo}\n" >> ${file}
done