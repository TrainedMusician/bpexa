import plotly
import plotly.graph_objs as go


def fileReader(fileName):
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return content


def chartCreator(data):
    hypotheticalList, labels = [[], []]
    for organism in data:
        hypotheticalList.append(organism.strip("\n").split("\t")[0])
        labels.append(organism.strip("\n").split("\t")[1])
    return [hypotheticalList, labels]


if __name__ == "__main__":
    # content = fileReader("statsProteins.txt")
    content = fileReader("statsEC.txt")
    hypothetical = chartCreator(content)

    data = chartCreator(content)
    layout = go.Layout(
        title='Indicatie geannoteerde eiwtten per organisme',
        width = 800,
        height = 400,
        font=dict(
            size=16
        ),
        legend=dict(
            font=dict(
                size=20
            )
        ),
        radialaxis=dict(
            ticksuffix='%'
        ),
        orientation=0
    )
    fig = go.Pie(labels=data[0], values=data[1])
    plotly.offline.plot([fig], filename='Pie geannoteerde eiwitten.html')