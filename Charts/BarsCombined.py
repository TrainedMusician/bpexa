import plotly
import plotly.graph_objs as go


def fileReader(fileName):
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return sorted(content)


def chartCreator(data):
    hypotheticalList, annotatedList, hypotheticalListPatric, \
    annotatedListPatric, labels = [[], [], [], [], []]
    for organism in data:
        annotatedList.append(int(organism.strip("\n").split()[1])+int(organism.strip("\n").split()[2]))
        hypotheticalList.append(organism.strip("\n").split()[2])
        annotatedListPatric.append(int(organism.strip("\n").split()[3])+int(organism.strip("\n").split()[4]))
        hypotheticalListPatric.append(organism.strip("\n").split()[4])
        labels.append(organism.strip("\n").split()[0])


    hypothetical = go.Bar(
        width=0.4,
        offset=-0.4,
        x=labels,
        y=hypotheticalList,
        name='Hypothetical',
        marker=dict(color='rgb(0,255,0)')
    )
    hypotheticalPatric = go.Bar(
        width=0.4,
        offset=0.0,
        x=labels,
        y=hypotheticalListPatric,
        name='HypotheticalPatric',
        marker=dict(color='rgb(0,120,255)')
    )
    annotated = go.Bar(
        width=0.4,
        offset=-0.4,
        x=labels,
        y=annotatedList,
        name='Annotated',
        marker=dict(color='rgb(255,60,0)')
    )
    annotatedPatric = go.Bar(
        width=0.4,
        offset=0.0,
        x=labels,
        y=annotatedListPatric,
        name='AnnotatedPatric',
        marker=dict(color='rgb(255,130,0)')
    )

    return [annotated, annotatedPatric, hypothetical, hypotheticalPatric]

if __name__ == "__main__":
    fileOut = open("combinedStats.txt")
    content = fileOut.readlines()
    fileOut.close()
    data = chartCreator(content)
    layout = go.Layout(
        barmode='group',
        xaxis=dict(type='category', title="Organism code"),
        yaxis=dict(title="Protein count")

    )
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='Barchart_annotated_vs_hypothetical_combined_annotated.html')