import plotly
import plotly.graph_objs as go
from GenBankParser import GeneBankParser
from collections import defaultdict


def fileReader(fileName):
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return sorted(content)


def heatmapCreator(organisms, data):
    ECs = {}
    for organismData in data:
        for ECNumber in [item for sublist in data[organismData] for item in
                         sublist]:
            organisms[organismData][".".join(ECNumber.split(".")[:3])] = \
            organisms[organismData][
                ".".join(
                    ECNumber.split(
                        ".")[
                    :3])] + 1
            ECs[".".join(ECNumber.split(".")[:3])] = None
    finalOrganisms = []
    finalECs = []
    finalCount = []
    # for organism in organisms:
    #     finalOrganisms.append(organism)
    #     countList = []
    #     for EC in organisms[organism]:
    #         finalECs.append(EC)
    #         countList.append(organisms[organism][EC])
    #     finalCount.append(countList)
    #
    #
    for EC in sorted(ECs):
        countList = []
        for organism in organisms:
            finalOrganisms.append(organism)
            finalECs.append(EC)
            countList.append(organisms[organism][EC])
        finalCount.append(countList)

    # print(len(finalOrganisms))
    # print(len(finalECs))
    # print(len(finalCount))
    trace = go.Heatmap(
        z=finalCount,
        x=finalOrganisms,
        y=finalECs)

    return trace


if __name__ == "__main__":
    organisms = {}
    data = {}
    parser = GeneBankParser()
    for organism in ["UA159", "GS-5", "NG8", "SVGS_061", "LAR01", "LJ23",
                     "NCTC10449", "NN2025", "UA159-FR"]:
        organisms[organism] = defaultdict(lambda: 0)
        data[organism] = parser.getECFrom("genbankfiles/%s.gb.txt" % organism)
    data = heatmapCreator(organisms, data)
    layout = dict(xaxis=dict(title="Organism genome"),
                  yaxis=dict(title="EC number"))

    plotly.offline.plot(dict(data=[data], layout=layout),
                        filename=
                        'Heatmap_annotated_vs_hypothetical_upped.html')
