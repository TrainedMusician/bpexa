
ALWAYS pull the content first if you are going to do something. Execute the 'git pull' command within your git-directory to have the latest content

---

## Edit a file
1. Execute 'git pull' for up-to-date files
2. Do the changes you want to do
3. Go to your (git) terminal
4. Execute 'git status'
5. Execute 'git add .' or 'git add <filenames>'
6. Execute 'git commit -m "<comment on what you did>"'
7. Execute 'git push'

Note: please only use 'git add .' if you are sure not to overwrite other files you didn't change. If you are not sure, use the specific 'git add <filename>' command

---

## Create a file

1. Execute 'git pull' for up-to-date files
2. Create the files you want
3. Go to your (git) terminal
4. Execute 'git status'
5. Execute 'git add .' or 'git add <filenames>'
6. Execute 'git commit -m "<comment on what you did>"'
7. Execute 'git push'

Note: please only use 'git add .' if you are sure not to overwrite other files you didn't change. If you are not sure, use the specific 'git add <filename>' command