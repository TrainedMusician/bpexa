import plotly
import plotly.graph_objs as go


def fileReader(fileName):
    """
    This function will open the given file and read it out. If there isn't a
    file with this name present it will return an empty string. If there is a
    file with the given name, this will be return sorted.
    :param fileName: file to open, String.
    :return: sorted list, content of file.
    """
    try:
        fileOut = open(fileName)
        file_content = fileOut.readlines()
        fileOut.close()
    except IOError:
        file_content = ''
    return sorted(file_content)


def chartCreator(data):
    """
    This function will visualise the bar chart by the data that is given. This
    figure will be returned as a list.
    :param data: list of the combined statistics of the genbank files.
    :return: list which contains the data for the bars. This will be combined
    with a layout to plot.
    """
    anotatedList, hypotheticalList, annotatedListPatric, \
        hypotheticalListPatric, labels = [[], [], [], [], []]
    for organism in data:
        hypotheticalList.append(organism.strip("\n").split()[2])
        anotatedList.append(int(organism.strip("\n").split()[2])-int(organism.strip("\n").split()[1]))
        hypotheticalListPatric.append(int(organism.strip("\n").split()[4]))
        annotatedListPatric.append(int(organism.strip("\n").split()[4])-int(organism.strip("\n").split()[3]))
        labels.append(organism.strip("\n").split()[0])

    annotated = go.Bar(
        width=0.4,
        offset=-0.4,
        x=labels,
        y=anotatedList,
        name='AnnotatedNCBI',
        marker=dict(color='rgb(0,255,0)')
    )
    anotatedPatric = go.Bar(
        width=0.4,
        offset=0.0,
        x=labels,
        y=annotatedListPatric,
        name='AnnotatedPatric',
        marker=dict(color='rgb(0,120,255)')
    )
    hypothetical = go.Bar(
        width=0.4,
        offset=-0.4,
        x=labels,
        y=hypotheticalList,
        name='HypotheticalNCBI',
        marker=dict(color='rgb(255,60,0)')
    )
    hypotheticalPatric = go.Bar(
        width=0.4,
        offset=0.0,
        x=labels,
        y=hypotheticalListPatric,
        name='HypotheticalPatric',
        marker=dict(color='rgb(255,130,0)')
    )

    return [hypothetical, hypotheticalPatric, annotated, anotatedPatric]


if __name__ == "__main__":
    data = chartCreator(fileReader("combinedStatsFlipped.txt"))
    layout = go.Layout(
        barmode='group',
        xaxis=dict(type='category', title="Organism code"),
        yaxis=dict(title="Protein count")

    )
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='Barchart_annotated_vs_hypothetical_combined_annotated_flipped.html')