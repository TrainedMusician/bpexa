import plotly
import plotly.graph_objs as go
from GenBankParser import GeneBankParser
from collections import defaultdict


def fileReader(fileName):
    """
    This function will open the given file and read it out. If there isn't a
    file with this name present it will return an empty string. If there is a
    file with the given name, this will be return sorted.
    :param fileName: file to open, String.
    :return: sorted list, content of file.
    """
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return sorted(content)


def heatmapCreator(organisms, data):
    """
    This function parses the organisms and the data to create the correct
    blocks for the heatmap
    :param organisms: defaultdictionary to put all the EC's in.
    :param data: the read EC numbers, dictionary.
    :return: trace, the heatmap data which will need to be combined with a
    layout to plot
    """
    ECs = {}
    for organismData in data:
        for ECNumber in [item for sublist in data[organismData] for item in
                         sublist]:
            organisms[organismData][ECNumber] = organisms[organismData][
                                                    ECNumber] + 1
            ECs[ECNumber] = None
    finalOrganisms = []
    finalECs = []
    finalCount = []
    for EC in sorted(ECs):
        countList = []
        for organism in organisms:
            finalOrganisms.append(organism)
            finalECs.append(EC)
            countList.append(organisms[organism][EC])
        finalCount.append(countList)
    trace = go.Heatmap(
        z=finalCount,
        x=finalOrganisms,
        y=finalECs)

    return trace


if __name__ == "__main__":
    """
    Organisms and data will be initialized, the organisms will get a default 
    dict per read organism. The data will be processed, layout will be added
    and it will be exported to the heatmap html page
    """
    organisms = {}
    data = {}
    parser = GeneBankParser()
    for organism in ["GS-5", "NG8", "UA159", "lar01", "lj23",
                     "nctc10449", "nn2025"]:
        organisms[organism] = defaultdict(lambda: 0)
        data[organism] = parser.getECFrom("newgenbankfiles/%s.gb.txt" % organism)
    data = heatmapCreator(organisms, data)
    layout = dict(xaxis=dict(title="Organism genome"),
                  yaxis=dict(title="EC number"))

    plotly.offline.plot(dict(data=[data], layout=layout),
                        filename=
                        'Heatmap_annotated_vs_hypothetical.html')
