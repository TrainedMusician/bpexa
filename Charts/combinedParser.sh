#!/usr/bin/env bash

file="combinedParserStats.txt"

[ -e ${file} ] && rm ${file}

FILES=newGenBankFiles/*
PATRICFILES=../annotations/*
for f in ${FILES}
do
  basename="$(cut -d'/' -f2 <<<"$f")"
  echo "Processing ${basename} file..."
  # take action on each file. $f store current file name
  total=$(cat "${f}" | egrep "/product=" | wc -l)
  hypo=$(cat "${f}" | egrep "/product=.*hypothetical.*" | wc -l)
  name="$(cut -d'.' -f1 <<<"${basename}")"
  # get numbers from the patric files
  patrictotal=$(cat "PATRICFiles/${basename}" | egrep "/product=" | wc -l)
  patrichypo=$(cat "PATRICFiles/${basename}" | egrep "/product=.*hypothetical.*" | wc -l)

  # format right for the next script
  printf "${name}\t${hypo}\t${total}\t${patrichypo}\t${patrictotal}\n" >> ${file}
done
