import plotly
import plotly.graph_objs as go


def fileReader(fileName):
    try:
        fileOut = open(fileName)
        content = fileOut.readlines()
        fileOut.close()
    except IOError:
        content = ''
    return sorted(content)


def chartCreator(data):
    hypotheticalList, annotatedList, labels = [[], [], []]
    for organism in data:
        annotatedList.append(int(organism.strip("\n").split()[1])-int(
            organism.strip("\n").split()[2]))
        hypotheticalList.append(organism.strip("\n").split()[2])
        labels.append(organism.strip("\n").split()[0])

    annotated = go.Bar(
        x=labels,
        y=annotatedList,
        name='Annotated',
        marker=dict(color='rgb(0,255,0)')
    )
    hypothetical = go.Bar(
        x=labels,
        y=hypotheticalList,
        name='Hypothetical',
        marker=dict(color='rgb(255,0,0)')
    )
    return [annotated, hypothetical]


if __name__ == "__main__":
    content = fileReader("stats.txt")
    hypothetical = chartCreator(content)

    data = chartCreator(content)
    layout = go.Layout(
        barmode='stack',
                   xaxis=dict(type='category', title="Organism code"),
        yaxis=dict(title="Protein count")

    )
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='Barchart_annotated_vs_hypothetical.html')
